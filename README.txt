$Id:

Region assign
-------------
It is a module inspired on Block Assign, but to let you take full control of
regions by URL regexp patterns or evaluation of arg(x) via drupal_eval.


INSTALLATION
------------
1. Add following code to your template.php

  Option a) First time implementation of hook_blocks:
  
  function phptemplate_blocks($region) {
    if (region_assign_show('THEMENAME', $region)) {
      return theme_blocks($region);
    }
  }

  Option b) If already have hook_block, perhaps you should use this way:
  
  function phptemplate_blocks($region) {
    if (region_assign_show('guianovios', $region))
      return '';
    
    YOUR STUFF HERE
    
  }

2. Activate Region Assign at administer -> site building -> modules

3. Create assignations at administer -> site building -> Themes -> Region Assign
